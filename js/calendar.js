/**
 * Created by me for myself
 * File: calendar.js
 * User: xvahal02@stud.fit.vutbr.cz
 * Date: 2/20/13
 * Time: 10:09 PM
 */

"use strict";

/**
 * Months, Days - Can be used for translation
 * @type {Object}
 */
var months = {
        0:"Leden",
        1:"Únor",
        2:"Březen",
        3:"Duben",
        4:"Květen",
        5:"Červen",
        6:"Červenec",
        7:"Srpen",
        8:"Září",
        9:"Říjen",
        10:"Listopad",
        11:"Prosinec"
    },
    days = {
        0:"Po",
        1:"Út",
        2:"St",
        3:"Čt",
        4:"Pá",
        5:"So",
        6:"Ne"
    };

/**
 * Construct the calendar
 * @param id
 * @param args
 * @return {*}
 * @constructor
 */

function Calendar(id, args) {
    /**
     * Info section
     * @type {Object}
     */
    var about = {
        Version:1.0,
        Author:"Tomas Vahalik"
    };

    /**
     * Default settings for basic calendar
     * @type {Object}
     */
    var defaults = {
        parentId: id,
        enabled: true,
        btnElement:"a",
        btnClass:"btn-append",
        btnLinkClass:"",
        btnCaption: "Calendar",
        wrapElement: "div",
        left: 70,
        top: 10,
        format: "dd-mm-yyyy",
        cssPath: "css/calendar.css"
    };

    if (id) {
        if (window === this) {
            return new Calendar(id, args);
        }
        // get main element
        this.parent = document.getElementById(id);

        // extend options from defaults and user attrs
        this.options = this.extend(defaults, args);


        // get head and put there a style definition if not defined yet
        var links = document.getElementsByTagName('link'),
            header = document.getElementsByTagName('head')[0],
            css = false;

        for (var i = links.length - 1; i >= 0; i--) {
            if(links[i].getAttribute('href') === this.options.cssPath)
            {
                css = true;
                break;
            }
        }
        if(css == false){

            var style=document.createElement("link");
            style.setAttribute("rel", "stylesheet");
            style.setAttribute("type", "text/css");
            style.setAttribute("href", this.options.cssPath);

            header.appendChild(style);
        }

        // create button
        var btn = this.constructBtn(),
        position = this.getPosition(this.parent);
        this.options.left += position.left;
        this.options.top += position.top;
        // init button to html
        this.insertAfter(btn, this.parent);
        this.insertAfter(this.constructTable(),btn);

        if(!this.options.enabled){
            this.parent.disabled = 'disabled'; //setAttribute('disabled','disabled')
        }
        this.parent.setAttribute('data',this.options.format);

        if(this.parent.value){
            this.fromDate = this.parseDate(this.options.parentId);
        }else{
            this.fromDate = new Date();
        }
        this.wrapper = document.getElementById(id + '-wrapper').style.visibility;

        this.fillCalendar(this.options.parentId,this.fromDate.getFullYear(),this.fromDate.getMonth());

        return this;
    } else {
        return about;
    }

}

/**
 * Extend Calendar Object
 * @type {Object}
 */
Calendar.prototype = {

    /**
     * Fill table with proper data
     * @param year
     * @param month
     * @return {*}
     */
    fillCalendar : function(id, year, month){
        var i = 1,
            j = 1,
            table = document.getElementById(id + '-tbody'),
            row = document.createElement('tr'),
            today = new Date(year,month,1),
            lastDay = new Date(year,month+1,0),
            startDay = today.getDay(),
            thisYear = today.getFullYear(),
            monthData = document.getElementById(id + '-month');

        if(startDay === 0){
            startDay = 7;
        }

        if(table){
            table.innerHTML = '';
            monthData.innerHTML = '';
            monthData.innerHTML = months[today.getMonth()] + " " + thisYear;
            monthData.setAttribute('data-month', today.getMonth());
            monthData.setAttribute('data-year', thisYear);
            table.appendChild(row);
            for(j; j < startDay; j++){
                row.appendChild(this.addCell(null, false));
            }

            while(i <= lastDay.getDate()){
                if((i + startDay -1 ) % 7 === 1){
                    row = document.createElement('tr');
                    table.appendChild(row);
                }
                row.appendChild(this.addCell(i,true));
                i++;
            }
        }
        return this;
    },

    /**
     * Set next month
     * @return {*}
     */
    nextMonth : function(){
        var id = Calendar.prototype.getRealId(this.parentNode.parentNode.id),
            actualMonth = parseInt(document.getElementById(id + '-month').getAttribute('data-month'),10),
            actualYear =  parseInt(document.getElementById(id + '-month').getAttribute('data-year'),10);

        if(actualMonth === 11){
            actualYear += 1;
            actualMonth = 0;
        }else{
            actualMonth += 1;
        }

        Calendar.prototype.fillCalendar(id,actualYear,actualMonth);

        return this;
    },
    /**
     * Get the previous month according to actual date
     * @return {Setting up new date to calendar}
     */
    prevMonth : function(){
        var id = Calendar.prototype.getRealId(this.parentNode.parentNode.id),
            actualMonth = parseInt(document.getElementById(id + '-month').getAttribute('data-month'),10),
            actualYear =  parseInt(document.getElementById(id + '-month').getAttribute('data-year'),10);

        if(actualMonth === 0){
            actualYear -= 1;
            actualMonth = 11;
        }else{
            actualMonth -= 1;
        }

        Calendar.prototype.fillCalendar(id,actualYear,actualMonth-1);
    },
    /**
     * Hide calendar
     * @param  {int} id
     * @return {*}
     */
    hide:function(id) {
        var what = document.getElementById(Calendar.prototype.getRealId(id) + '-wrapper');
        what.style.visibility = 'hidden';
        return this;
    },
    /**
     * Show calendar
     * @param  {int} id
     * @return {*}
     */
    show:function(id) {
        var parentId = Calendar.prototype.getRealId(id),
            input = document.getElementById(parentId);
        if(input.value){
            var fromDate = this.parseDate(parentId);
            if(fromDate === null){
                fromDate = new Date();
            }
            Calendar.prototype.fillCalendar(parentId,fromDate.getFullYear(),fromDate.getMonth());
        }
        var what = document.getElementById(parentId + '-wrapper');
        what.style.visibility = 'visible';
        return this;
    },
    /**
     * Show/hide calendar
     * @return {*}
     */
    toggle:function () {
        var id = Calendar.prototype.getRealId(this.id);

        if(document.getElementById(id + '-wrapper').style.visibility === 'hidden'){
            Calendar.prototype.show(id);
        }else{
            Calendar.prototype.hide(id);
        }

        return this;
    },
    /**
     * Insert what node after another node
     * @param what
     * @param after
     * @return {*}
     */
    insertAfter:function (what, after) {
        after.parentNode.insertBefore(what, after.nextSibling);
        return this;
    },
    /**
     * Create object from two, first with defaults, second update
     * @param {Object} obj1
     * @param {Object} obj2
     * @return {Object}
     */
    extend:function (obj1, obj2) {
        var outObj = Object(obj1);

        for (var key in obj2) {
            outObj[key] = obj2[key];
        }
//        console.log("my object: %o", outObj);
        return outObj;
    },
    /**
     * Returns offset of given element
     * @param el element or ID
     * @return {Object} [left and top position]
     */
    getPosition : function (el) {
        var x = 0,
            y = 0;

        if (el.offsetParent) {
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)){
                x += el.offsetLeft;
                y += el.offsetTop;
                el = el.offsetParent;
            }
        }
        return { left: x, to:y };
    },
    /**
     * Create init button for calendar
     * @return {Element}
     */
    constructBtn: function() {
        var btn = document.createElement(this.options.btnElement);
        btn.id = this.options.parentId + '-a';
        btn.className = this.options.btnClass;
        btn.href = "#";
        btn.onclick = this.toggle;

        var txt = document.createElement("span");
        txt.className = this.options.btnLinkClass;
        txt.innerHTML = this.options.btnCaption;
        btn.appendChild(txt);

        return btn;
    },
    /**
     * Set date to calendar
     * @return {*}
     */
    setDate : function() {

        var day = parseInt(this.innerText,10),
            id = Calendar.prototype.getRealId(this.parentNode.parentNode.parentNode.id),
            month = parseInt(document.getElementById(id + '-month').getAttribute('data-month'),10) + 1,
            year =  parseInt(document.getElementById(id + '-month').getAttribute('data-year'),10),
            input = document.getElementById(id),
            formatObj = Calendar.prototype.getFormat(input.getAttribute('data')),
            out = '',
            newDate = {d: day,m:month,y:year};

        for(var i = 1; i < 6; i++){
            for(var what in formatObj){
                if(formatObj[what] === i){
                    out += newDate[what];
                }
            }
            if(i === 2 || i === 4){
                out += formatObj.sep;
            }
        }

        input.value = out;

        Calendar.prototype.hide(id);

        return this;
    },
    /**
     * Adds one <td> with day number | adds one empty cell
     * @param {int} num
     * @param {boolean} type
     * @return {Element}
     */
    addCell : function(num, type) {
        var cell = document.createElement('td'),
            href = document.createElement('a');

        if(type === false){
            cell.className = 'disabled';
        }
        else{
            cell.className = 'day';
        }

        href.href = "#";
        href.onclick = this.setDate;
        href.innerHTML = num;

        cell.appendChild(href);

        return cell;

    },
    /**
     * Constructs new table for calendar
     * @return {Element}
     */
    constructTable : function() {
        var mainDiv = document.createElement(this.options.wrapElement);
        mainDiv.id = this.options.parentId + '-wrapper';
        mainDiv.className = 'wrapper';
        mainDiv.style.visibility = "hidden";
        mainDiv.style.position = "absolute";
//        console.log(this.options.left);
        mainDiv.style.top = this.options.top + "px";
        mainDiv.style.left = this.options.left + "px";

        var prev = document.createElement('a'),
            next = document.createElement('a'),
            mont = document.createElement('div');

        mont.id = this.options.parentId +"-month";
        mont.className = 'center';

        prev.href = next.href = '#';
        prev.innerText = '<';
        next.innerText = '>';
        prev.onclick = this.prevMonth;
        prev.className = 'left';
        next.className = 'right';
        next.onclick = this.nextMonth;

        var controls = document.createElement('div');
        controls.className = 'controls';
        controls.appendChild(prev);
        controls.appendChild(mont);
        controls.appendChild(next);

        var tbl = "<table id='"+this.options.parentId+"-table'>" +
            "<thead>" +
            "<tr>";
        for(var i = 0; i<7;i++){
            tbl += "<th>" + days[i] + "</th>";
        }
        tbl += "</tr>" +
            "</thead>" +
            "<tbody id='" + this.options.parentId + "-tbody'>" +
            "</tbody>" +
            "</table>";

        mainDiv.innerHTML = tbl;

        mainDiv.appendChild(controls);
        return mainDiv;
    },
    /**
     * Get ID of parent
     * @param  {String} id
     * @return {String}
     */
    getRealId : function (id){
        var arr = id.split('-'),
            retId = arr[0];
        return retId;
    },
    /**
     * Parse date
     * @param  {String} id
     * @return {Date} [In desired format] | {null} [if format is not supported]
     */
    parseDate : function (id) {
        var parent = document.getElementById(Calendar.prototype.getRealId(id)),
            formatObj = Calendar.prototype.getFormat(parent.getAttribute('data')),
            value = parent.value,
            pattern = new RegExp(/([0-9]+)([\\\/\-\.\s]){1,2}([0-9]+)([\\\/\-\.\s]){1,2}([0-9]+)/gi),
            match;

        match = pattern.exec(value);

        return (match) ? new Date(match[formatObj.y],match[formatObj.m]-1,match[formatObj.d]) : null;

    },
    /**
     * Get format from pre-configured
     * @param  {String} value
     * @return {Object} [Format saved in object where position of dym and sep is saved]
     */
    getFormat : function (value) {
    //var format = document.getElementById(Calendar.prototype.getRealId(id)),
        var pattern = new RegExp(/([dmy]+)([\\\/\-\.\s]){1,2}([dmy]+)([\\\/\-\.\s]){1,2}([dmy]+)/gi),
            match,
            result = {d:null,m:null,y:null,sep:null};

        match = pattern.exec(value);

        var position = 1;

        for(position = 1; position < match.length; position++){
            if(match[position][0] === 'd'){
                result.d = position;
            }else if(match[position][0] === 'm'){
                result.m = position;
            }else if(match[position][0] === 'y'){
                result.y = position;
            }
            if(position === 2){
                result.sep = match[position][0];
            }
        }

        return result;
    }

};
